const keys = "UP,DOWN,RIGHT,LEFT,SPACE,W,A,S,D,R"
let pl, k, jump, score, music

const randint = lim => Math.floor(Math.random() * lim)
const randring = (min,max) => randint(max-min+1) + min
const rX = () => randint(game.config.width)
const rY = () => randint(game.config.height)

class Main extends Phaser.Scene {

    init() {
        score = 0
    }



    preload() {
        this.load.audio("coins", "./assets/snd/coins.wav")
        this.load.audio("dead", "./assets/snd/dead.wav")
        this.load.audio("bump", "./assets/snd/bump.wav")
        this.load.audio("bgsnd", "./assets/snd/background.mp3")
        this.load.audio("jump", "./assets/snd/jump.wav")

        
        this.load.image('die', './assets/img/deaths.png')
        this.load.image('bg', './assets/img/backgwond.png')
        this.load.image('pl', './assets/img/dude.png')
        this.load.image('bad', './assets/img/bad.png')
        this.load.image('badd', './assets/img/badd.png')
        this.load.image('coin', './assets/img/coin.png')
        this.load.image('plat', './assets/img/platform.png')

    }

    create() {

        this.add.image(0, 0, 'bg').setOrigin(0, 0)
        pl = this.physics.add.sprite(110, 110, 'pl')
        pl.setCollideWorldBounds(true)
        pl.setScale(3.5)
        pl.setGravityY(200)

        let scoreText = this.add.text(16,16, 'Score: 0', {
            color: 'red',
            fontSize: 30,
        })


        let bad = this.physics.add.sprite(300, 110, 'bad')
        bad.setCollideWorldBounds(true)
        bad.setScale(2)
        bad.setVelocity(300)
        bad.setBounce(1.1)

        let plats = this.physics.add.staticGroup()
        plats.create(340, 280, "plat")
        plats.create(100, 200, "plat")
        plats.create(400, 600, "plat")


         music = this.sound.add('bgsnd')
        let pick_coin = this.sound.add('coins')
        let bump = this.sound.add('bump')
        let gameover = this.sound.add('dead')
        let jump = this.sound.add('jump')

        music.play()

        let coin = this.physics.add.group({
            key: "coin",
            repeat: 4,
            setXY: { x: 10, y: 0, stepX: 34 }
        })

        const spawnCoins = () => coin.create(rX(), rY(), 'coin')

        coin.children.iterate(child => {
            child.setGravityY(500)
            child.setCollideWorldBounds(true)
            k = this.input.keyboard.addKeys(keys)
        })

        const collideBad = (p,c) => {
            this.physics.pause()
            music.stop()
            p.setTint('#FF0000')
            this.add.image(0, 0, 'die').setOrigin(0,0)
        }

        this.physics.add.collider(pl, plats)
        this.physics.add.collider(pl, bad, collideBad)
        this.physics.add.collider(coin, plats)
        this.physics.add.collider(pl, coin, (p, c) => {
            c.destroy()
            pick_coin.play()
            spawnCoins()
            score += 1
            scoreText.setText(`Score: ${score}`)
        })
        this.physics.add.collider(coin, coin)
    }





    update() {

   if (k.R.isDown) {
                this.scene.restart()
                music.stop()

            }

        if (k.LEFT.isDown || k.A.isDown) {
            pl.setVelocityX(-400)
        }

        else if (k.RIGHT.isDown || k.D.isDown) {
            pl.setVelocityX(400)
        }


        if (pl.body.onFloor()) {


            if (k.UP.isDown || k.W.isDown) {
                pl.setVelocityY(-400)
            }

            if (! pl.body.onFloor()) {
                pl.setDragX(0)
            }

         

        }
    }
}


let game = new Phaser.Game({
    scene: Main,
    physics: { default: 'arcade' },
    pixelArt: true,
})