let pl, curs

new Phaser.Game({
  width: 683, //half 1366
  height: 384, // half 768
  physics: {
    default: "arcade",
    arcade: {
      gravity: { y: 500, x: 0},
      debug: false
    }
  },
  scene: {
    preload: function () {      
      this.load.image('background', './assets/backgwond.png')
      this.load.image('player', './assets/dude.png')
      this.load.image('plat', './assets/platform.png')
    },
    create() {
      this.add.image(0, 0, "background").setOrigin(0,0)
     pl = this.physics.add.sprite(100, 100, "player")
     pl.body.collideWorldBounds = true
     pl.body.bounce = {x:0,y:.4}
     pl.setDragX(1200)
     curs = this.input.keyboard.createCursorKeys()
     plats = this.physics.add.staticGroup()
     foo = plats.create(60,175,'plat')
     //foo.setScale(10)  //makes bigger /not really its fake/
      foo.body.rotation = 30
     this.physics.add.collider(pl,plats)
    },

    update() {
      if (curs.left.isDown) {
        pl.setVelocityX(-300)
      } else if (curs.right.isDown) {
        pl.setVelocityX(300)
      }
      if (pl.body.onFloor() && curs.up.isDown) {
        pl.setVelocityY(-500)
      }
      if (!pl.body.onFloor()) {
        pl.setDragX(0)
      } else {
        pl.setDragX(1200)
      }
    }
  }
})