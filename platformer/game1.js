

function preload() {

  this.load.audio("coin","/assets/snd/coins.wav")
  this.load.audio("dead","/assets/snd/dead.wav")
  this.load.audio("bump","/assets/snd/bump.wav")
  this.load.audio("bgsnd","/assets/snd/background.wav")
  this.load.audio("jump","/assets/snd/jump.wav")

  this.load.image('plat', './assets/img/platform.png')
  this.load.image('bg', './assets/img/backgwond.png')
  this.load.image('pl', './assets/img/dude.png')
  this.load.image('bad', './assets/img/bad.png')
  this.load.image('badd', './assets/img/badd.png')
  this.load.image('coin', './assets/img/coin.png')
}

function create() {
  this.add.image(0,0, 'bg').setOrigin(0,0)
  pl = this.physics.add.sprite(110,110, 'pl')
  pl.setCollideWorldBounds(true)
  pl.setScale(1)
  pl.setGravityY(600)

  bad = this.physics.add.sprite(110,110, 'bad')
  bad.setCollideWorldBounds(true)
  bad.setScale(1)
  bad.setVelocity(300)
  bad.setBounce(1)

  plats = this.physics.add.staticGroup()
  plats.create(340,280,"plat")
  plats.create(100,170,"plat")

  let music = this.sound.add('bgsnd')
  let pick_coin = this.sound.add('coin')
  let hit = this.sound.add('bump')
  let death = this.sound.add('dead')
  let up = this.sound.add('jump')

  coin = this.physics.add.group({
    key: "coin",
    repeat: 20,
    setXY: { x: 10, y: 0, stepX: 34}
  })
  coin.children.iterate( child => {
    child.setGravityY(500)
    child.setCollideWorldBounds(true)
24
24
  k = this.input.keyboard.addKeys(keys)

  this.physics.add.collider(pl,plats)
  this.physics.add.collider(pl,bad)
  this.physics.add.collider(bad,plats)
  this.physics.add.collider(coin,plats)
  this.physics.add.collider(coin,pl, (p,c) => {
    c.destroy()
    pick_coin.play()
  })
  this.physics.add.collider(coin,coin)
  this.physics.add.collider(coin,bad)
}
const keys = "UP,DOWN,RIGHT,LEFT,SPACE,W,A,S,D"
let pl, k, jump

function preload() {

  this.load.audio("coin","/assets/snd/coins.wav")
  this.load.audio("dead","/assets/snd/dead.wav")
  this.load.audio("bump","/assets/snd/bump.wav")
  this.load.audio("bgsnd","/assets/snd/background.wav")
  this.load.audio("jump","/assets/snd/jump.wav")

  this.load.image('plat', './assets/img/platform.png')
  this.load.image('bg', './assets/img/backgwond.png')
  this.load.image('pl', './assets/img/dude.png')
  this.load.image('bad', './assets/img/bad.png')
  this.load.image('badd', './assets/img/badd.png')
  this.load.image('coin', './assets/img/coin.png')
}

function create() {
  this.add.image(0,0, 'bg').setOrigin(0,0)
  pl = this.physics.add.sprite(110,110, 'pl')
  pl.setCollideWorldBounds(true)
  pl.setScale(1)
  pl.setGravityY(600)

  bad = this.physics.add.sprite(110,110, 'bad')
  bad.setCollideWorldBounds(true)
  bad.setScale(1)
  bad.setVelocity(300)
  bad.setBounce(1)

  plats = this.physics.add.staticGroup()
  plats.create(340,280,"plat")
  plats.create(100,170,"plat")

  let music = this.sound.add('bgsnd')
  let pick_coin = this.sound.add('coin')
  let hit = this.sound.add('bump')
  let death = this.sound.add('dead')
  let up = this.sound.add('jump')

  coin = this.physics.add.group({
    key: "coin",
    repeat: 20,
    setXY: { x: 10, y: 0, stepX: 34}
  })
  coin.children.iterate( child => {
    child.setGravityY(500)
    child.setCollideWorldBounds(true)
24
24
  k = this.input.keyboard.addKeys(keys)

  this.physics.add.collider(pl,plats)
  this.physics.add.collider(pl,bad)
  this.physics.add.collider(bad,plats)
  this.physics.add.collider(coin,plats)
  this.physics.add.collider(coin,pl, (p,c) => {
    c.destroy()
    pick_coin.play()
  })
  this.physics.add.collider(coin,coin)
  this.physics.add.collider(coin,bad)
}

function update() {

  if (k.LEFT.isDown || k.A.isDown) {
    pl.setVelocityX(-400)
  }

  else if (k.RIGHT.isDown || k.D.isDown) {
    pl.setVelocityX(400)
  }


  if (pl.body.onFloor()) {
    pl.setDragX(1200)

  
    if (k.UP.isDown || k.W.isDown) {
      pl.setVelocityY(-400)
      up.play()
    }

  }
}
