# BaseML CommanMark MarkDown

https://baseml.soilscr.org

## This is a paragraph thats just a super long sentence. do not put hard breaks in your paragraph

### one * for italics ** stars for bold *** for both

#=weird stuff

*hi*
**hi**
***hi***

do ***not*** mix them


# list

* this
* is
* unorderd

1. this
1. an
1. orderd
1. list

### dont put a 2 in the thing

### dont *multi* **levels**

# links

Just type **it**

https://skilstak.io

<dynodogdavid@gmail.com>

### Phone

<tel:555-555-5555>

or u can do [this](https://skilstak.io)


# Blocks

> whooo

# fences

```
stuuf
stuff
stuf
```

u can do this too

```js
console.log('hjasgdkuyHDJGASK')
```

look a this its the same key but with shift

~~~md
stuuf stuff more stuuf

```
hi
```


~~~


# seporators

four dashes

----
hi
----

----

----

----



# pics

donload

![weirdthing](./weirdthing.gif)

copy

![weirdthing](./weirdthing.gif)  (https://github.org)

